# v1.0.0-0
  * Refactor the whole API, remove a bunch of unused stuff
  * Upgrade dependencies

# v0.2.0
  * Move to controlled docker image (devteamreims/4ME#140)
  * Move to yarn as package manager (devteamreims/4ME#141)
  * Implement integration tests
  * Mute loggers in test env

# v0.1.4
  * Update dependencies
  * Use Jest for testing
  * Plug in gitlab build pipeline
  * Fix regression introduced in [#105](devteamreims/4ME#105)

# Version v0.1.3
  * Don't produce verbose log when using npm start
  * Add npm-check script and integrates with gitlab-ci [#115](devteamreims/4ME#115)/[#45](devteamreims/4ME#45)

# Version 0.1.2
  * Fix wrong sector group naming [#105](devteamreims/4ME#105)

# Version 0.1.1
  * Add version in /status [#91](devteamreims/4ME#91)
  * Add david-dm badge to README
  * Rename KHYR and XKHYR to KHR and XKHR [#95](devteamreims/4ME#95)
  * Add *EH block-layer sectors [#92](devteamreims/4ME#92)

# Version 0.1.0
  * Initial version
